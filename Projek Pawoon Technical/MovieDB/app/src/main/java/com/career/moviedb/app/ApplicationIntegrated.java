package com.career.moviedb.app;

import android.app.Application;

import com.career.moviedb.component.ApiComponent;
import com.career.moviedb.component.DaggerApiComponent;
import com.career.moviedb.module.ApiClientModule;
import com.career.moviedb.module.AppModule;
import com.career.moviedb.util.Constant;

public class ApplicationIntegrated extends Application {

    public static ApiComponent apiComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        apiComponent = DaggerApiComponent.builder()
                .appModule(new AppModule(this))
                .apiClientModule(new ApiClientModule(Constant.Api.BASE_URL))
                .build();
    }

    public static ApiComponent getComponent() {
        return apiComponent;
    }
}
