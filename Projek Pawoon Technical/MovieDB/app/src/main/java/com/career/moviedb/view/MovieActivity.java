package com.career.moviedb.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import retrofit2.Retrofit;

import android.app.Application;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import com.career.moviedb.R;
import com.career.moviedb.adapter.MovieAdapter;
import com.career.moviedb.app.ApplicationIntegrated;
import com.career.moviedb.da_sqlite.DA_M_Movie;
import com.career.moviedb.databinding.ActivityMovieBinding;
import com.career.moviedb.model.Movie;
import com.career.moviedb.viewmodel.MovieViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MovieActivity extends AppCompatActivity {

    @Inject
    Retrofit retrofit;
    @Inject
    Application application;
    private ActivityMovieBinding binding;
    public static ProgressDialog progressDialog;
    private MovieViewModel movieViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie);
        ((ApplicationIntegrated) getApplication()).getComponent().inject(this);
        binding.setLifecycleOwner(this);
        movieViewModel = ViewModelProviders.of(this).get(MovieViewModel.class);
        movieViewModel.getMovies();

        progressDialog = new ProgressDialog(this);
        showProgressDialog();
        observableChanges();

    }

    private void observableChanges() {
        movieViewModel.movieList.observe(this, responseMovie -> Recycler(responseMovie.getResults()));
        movieViewModel.movieListLocal.observe(this, responseMovie -> Recycler(responseMovie));
        movieViewModel.errorMessage.observe(this, s -> Toast.makeText(MovieActivity.this, s, Toast.LENGTH_LONG).show());
        progressDialog.dismiss();

        binding.nestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()){
                    showProgressDialog();
                    observableChanges();
                }
            }
        });

    }

    private void Recycler(ArrayList< Movie > movies) {
        binding.setAdapter(new MovieAdapter(movies, MovieActivity.this));
        binding.getAdapter().notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem menuItem = menu.findItem(R.id.toolbarSearch);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint("Search by title");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (!s.equals("")){
                    movieViewModel.getTitleSearch(s);
                    movieViewModel.movieListSearch.observe(MovieActivity.this, responseSearch -> Recycler(responseSearch));
                }else {
                    movieViewModel.getMovies();
                    observableChanges();
                }
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void showProgressDialog(){
        progressDialog.setTitle("Loading");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
}