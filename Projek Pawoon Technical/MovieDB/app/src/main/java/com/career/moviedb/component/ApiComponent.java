package com.career.moviedb.component;

import com.career.moviedb.view.MovieActivity;
import com.career.moviedb.module.ApiClientModule;
import com.career.moviedb.module.AppModule;
import com.career.moviedb.repository.MovieRepository;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApiClientModule.class, AppModule.class})
public interface ApiComponent {
    void inject(MovieActivity movieActivity);
    void injectMovie(MovieRepository movieRepository);
}
