package com.career.moviedb.biding_adapter;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.career.moviedb.util.Constant;

import androidx.databinding.BindingAdapter;

public class BindingAdapters {
    @BindingAdapter({"app:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext()).load(Constant.Api.BASE_IMAGE_URL + imageUrl).into(view);
    }
}
