package com.career.moviedb.api;

import com.career.moviedb.model.ResponseMovie;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    @GET("movie/now_playing")
    Call< ResponseMovie > getMovies(@QueryMap Map<String , String> queryParameters);
}
