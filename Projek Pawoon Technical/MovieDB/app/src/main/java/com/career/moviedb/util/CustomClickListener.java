package com.career.moviedb.util;

import com.career.moviedb.model.Movie;

public interface CustomClickListener {
    void cardClicked(Movie f);
}
