package com.career.moviedb.util;

public interface Constant {

    interface Api {
        String BASE_URL = "https://api.themoviedb.org/3/";
        String TOKEN_NAME = "api_key";
        String TOKEN_VALUE = "ab5b8ce0b0bc1d0322483dac61466187";
        String BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w500";
    }

}
