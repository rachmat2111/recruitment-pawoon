package com.career.moviedb.db_sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.career.moviedb.da_sqlite.DA_M_Movie;

public class DB_sqlite_helper extends SQLiteOpenHelper {

    public static final String DB_NAME = "movie.db";
    public static final int   DB_VERSION = 7;

    DB_sqlite_helper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createDatabase(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        upgradeDatabase(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void createDatabase(SQLiteDatabase db){
        db.execSQL(DA_M_Movie.STATEMENT_CREATE);
    }

    public void upgradeDatabase(SQLiteDatabase db){
        db.execSQL(DA_M_Movie.STATEMENT_DROP);
        onCreate(db);
    }
}
