package com.career.moviedb.viewmodel;

import android.app.Application;
import android.util.Log;

import com.career.moviedb.da_sqlite.DA_M_Movie;
import com.career.moviedb.model.Movie;
import com.career.moviedb.model.ResponseMovie;
import com.career.moviedb.repository.MovieRepository;
import com.career.moviedb.util.Constant;
import com.career.moviedb.util.InternetConnectivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import static com.career.moviedb.view.MovieActivity.progressDialog;

public class MovieViewModel extends AndroidViewModel {
    public MutableLiveData< ResponseMovie > movieList = new MutableLiveData<>();
    public MutableLiveData< ArrayList<Movie> > movieListLocal = new MutableLiveData<>();
    public MutableLiveData< ArrayList<Movie> > movieListSearch = new MutableLiveData<>();
    public MutableLiveData<String> errorMessage = new MutableLiveData<>();
    public MutableLiveData<Boolean> showLoadingProg = new MutableLiveData<>();
    private MovieRepository movieRepository = new MovieRepository();
    private boolean isConnected;

    public MovieViewModel(@NonNull Application application) {
        super(application);
    }

    public void getMovies() {
        isConnected = InternetConnectivity.isNetworkAvailable(getApplication());
        if (isConnected) {
            showLoadingProg.setValue(false);
            Map<String, String> map = new HashMap<>();
            map.put(Constant.Api.TOKEN_NAME, Constant.Api.TOKEN_VALUE);
            movieList = movieRepository.getMovies(map);
        } else {
            showLoadingProg.setValue(false);
            movieListLocal = movieRepository.getListMovieLocal();
        }
    }

    public void getTitleSearch(String title){
        movieListSearch = movieRepository.getListSearch(title);
    }
}
