package com.career.moviedb.repository;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.career.moviedb.api.ApiInterface;
import com.career.moviedb.app.ApplicationIntegrated;
import com.career.moviedb.da_sqlite.DA_M_Movie;
import com.career.moviedb.model.Movie;
import com.career.moviedb.model.ResponseMovie;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MovieRepository {
    @Inject
    Retrofit retrofit;
    @Inject
    Application application;
    private static MovieRepository repository;
    private ApiInterface apiInterface ;
    private DA_M_Movie da_m_movie;

    public static MovieRepository getInstance(){
        if (repository == null){
            repository = new MovieRepository();
        }
        return repository;
    }

    public MovieRepository(){
        ApplicationIntegrated.getComponent().injectMovie(this);
        apiInterface = retrofit.create(ApiInterface.class);
        da_m_movie = new DA_M_Movie(this.application);
    }

    public MutableLiveData< ResponseMovie > getMovies(Map<String , String> stringMap){
        MutableLiveData<ResponseMovie> responseMovieMutableLiveData = new MutableLiveData<>();
        apiInterface.getMovies(stringMap).enqueue(new Callback<ResponseMovie>() {
            @Override
            public void onResponse(Call<ResponseMovie> call, Response<ResponseMovie> response) {
                if (response.isSuccessful()){
                    responseMovieMutableLiveData.setValue(response.body());
                    da_m_movie.insert_or_replace(response.body().getResults());
                }
            }
            @Override
            public void onFailure(Call<ResponseMovie> call, Throwable t) {
                responseMovieMutableLiveData.setValue(null);
            }
        });
        return responseMovieMutableLiveData;
    }

    public MutableLiveData< ArrayList<Movie> > getListMovieLocal(){
        MutableLiveData<ArrayList<Movie>> responseMovieMutableLiveDataLocal = new MutableLiveData<>();
        if (da_m_movie.getList().size() > 0){
            responseMovieMutableLiveDataLocal.setValue(da_m_movie.getList());
        }else {
            Toast.makeText(application, "Tidak ada data di local storage", Toast.LENGTH_SHORT).show();
        }
        return responseMovieMutableLiveDataLocal;
    }

    public MutableLiveData< ArrayList<Movie> > getListSearch(String title){
        MutableLiveData<ArrayList<Movie>> responseMovieMutableLiveDataLocal = new MutableLiveData<>();
        if (da_m_movie.getList().size() > 0){
            responseMovieMutableLiveDataLocal.setValue(da_m_movie.getListSearch(title));
        }else {
            Toast.makeText(application, "Tidak ada judul film yg dicari", Toast.LENGTH_SHORT).show();
        }
        return responseMovieMutableLiveDataLocal;
    }
}
