package com.career.moviedb.da_sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.career.moviedb.db_sqlite.DB_Base;
import com.career.moviedb.model.Movie;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;

public class DA_M_Movie extends DB_Base {

    public static final String TABLE_NAME = "m_movie";
    public static final String STATEMENT_CREATE = "CREATE TABLE `" + TABLE_NAME + "` (" +
            "`vote_count` TEXT NOT NULL, `id` TEXT NOT NULL, " +
            "`vote_average` TEXT NOT NULL, " +
            "`title` TEXT NOT NULL, `popularity` TEXT NOT NULL, " +
            "`posterPath` TEXT NOT NULL, `originalLanguage` TEXT NOT NULL, " +
            "`original_title` TEXT NOT NULL, `backdropPath` TEXT NOT NULL, " +
            "`overview` TEXT NOT NULL, `release_date` TEXT NOT NULL, " +
            "PRIMARY KEY(`id`));";

    public static final String STATEMENT_DROP = "DROP TABLE IF EXISTS `" + TABLE_NAME + "`;";
    public static final String STATEMENT_INSERT_OR_REPLACE = "INSERT OR REPLACE INTO `" + TABLE_NAME + "` (" +
            "`vote_count`, `id`, `vote_average`, `title`, `popularity`, " +
            "`posterPath`, `originalLanguage`, `original_title`, `backdropPath`, " +
            "`overview`, `release_date`" +
            ") VALUES (" +
            "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?" +
            ");";

    public DA_M_Movie(Context context) {
        super(context);
    }

    public int insert_or_replace(ArrayList< Movie > vectors) {
        int rtn = 0;
        open();
        try {
            __sqliteDatabase.beginTransactionNonExclusive();
            SQLiteStatement sqliteStatement = __sqliteDatabase.compileStatement(STATEMENT_INSERT_OR_REPLACE);

            for(Movie soapObject : vectors) {
                int i = 1;

                sqliteStatement.bindString(i++, String.valueOf(soapObject.getVoteCount()));
                sqliteStatement.bindString(i++, String.valueOf(soapObject.getId()));
                sqliteStatement.bindString(i++, String.valueOf(soapObject.getVoteAverage()));
                sqliteStatement.bindString(i++, String.valueOf(soapObject.getTitle()));
                sqliteStatement.bindString(i++, String.valueOf(soapObject.getPopularity()));
                sqliteStatement.bindString(i++, String.valueOf(soapObject.getPosterPath()));
                sqliteStatement.bindString(i++, String.valueOf(soapObject.getOriginalLanguage()));
                sqliteStatement.bindString(i++, String.valueOf(soapObject.getOriginalTitle()));
                sqliteStatement.bindString(i++, String.valueOf(soapObject.getBackdropPath()));
                sqliteStatement.bindString(i++, String.valueOf(soapObject.getOverview()));
                sqliteStatement.bindString(i++, String.valueOf(soapObject.getReleaseDate()));

                sqliteStatement.executeInsert();
                sqliteStatement.clearBindings();
                rtn += 1;
            }

            __sqliteDatabase.setTransactionSuccessful();

        } catch(Exception e) {
            Log.d("TAG","onResponse getList gagal" + e.getMessage());

        } finally {
            __sqliteDatabase.endTransaction();
            close();
        }
        return rtn;
    }

    public ArrayList< Movie > getList() {
        ArrayList<Movie> myList=new ArrayList<>();
        open();

        try {
            String query = "SELECT *" +" FROM " + TABLE_NAME ;

            Cursor pos = __sqliteDatabase.rawQuery(query, null);
            while(pos.moveToNext()) {
                try {
                    int i = 0;
                    Movie rec = new Movie();

                    rec.voteCount = pos.getInt(i++);
                    rec.id = pos.getInt(i++);
                    rec.voteAverage = pos.getInt(i++);
                    rec.title = pos.getString(i++);
                    rec.popularity = pos.getDouble(i++);
                    rec.posterPath = pos.getString(i++);
                    rec.originalLanguage = pos.getString(i++);
                    rec.originalTitle = pos.getString(i++);
                    rec.backdropPath = pos.getString(i++);
                    rec.overview = pos.getString(i++);
                    rec.releaseDate = pos.getString(i++);

                    myList.add(rec);
                } catch(Exception e) {
                    Log.d("TAG","onResponse getList gagal" + e.getMessage());
                }
            }
            pos.close();
        } catch(Exception e) {
            Log.d("TAG","onResponse getList gagal" + e.getMessage());

        } finally {
            close();
        }
        return myList;
    }

    public ArrayList< Movie > getListSearch(String title) {
        ArrayList<Movie> myList=new ArrayList<>();
        open();

        try {
            String query = "SELECT *" +" FROM " + TABLE_NAME +" WHERE title LIKE '%"+title+"%'" ;
            Log.d("TAG","onResponse = "+query);
            Cursor pos = __sqliteDatabase.rawQuery(query, null);
            while(pos.moveToNext()) {
                try {
                    int i = 0;
                    Movie rec = new Movie();

                    rec.voteCount = pos.getInt(i++);
                    rec.id = pos.getInt(i++);
                    rec.voteAverage = pos.getInt(i++);
                    rec.title = pos.getString(i++);
                    rec.popularity = pos.getDouble(i++);
                    rec.posterPath = pos.getString(i++);
                    rec.originalLanguage = pos.getString(i++);
                    rec.originalTitle = pos.getString(i++);
                    rec.backdropPath = pos.getString(i++);
                    rec.overview = pos.getString(i++);
                    rec.releaseDate = pos.getString(i++);

                    myList.add(rec);
                } catch(Exception e) {
                    Log.d("TAG","onResponse getList gagal" + e.getMessage());
                }
            }
            pos.close();
        } catch(Exception e) {
            Log.d("TAG","onResponse getList gagal" + e.getMessage());

        } finally {
            close();
        }
        return myList;
    }
}
