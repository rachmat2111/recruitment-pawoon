package com.career.moviedb.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;

import com.career.moviedb.R;
import com.career.moviedb.databinding.ActivityDetailMovieBinding;
import com.career.moviedb.model.Movie;

public class DetailMovie extends AppCompatActivity {

    ActivityDetailMovieBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_movie);
        Intent intent = getIntent();
        if (intent != null) {
            Movie movie = intent.getParcelableExtra("Movie");
            binding.setMovie(movie);
        }
    }
}